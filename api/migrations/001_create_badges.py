steps = [
    [
        # Up migration
        """
        CREATE TABLE IF NOT EXISTS badges (
            id  SERIAL UNIQUE  NOT NULL,
            badge_name varchar(20)  NOT NULL,
            sprite_url varchar(200)  NOT NULL,
            CONSTRAINT pk_badges PRIMARY KEY (id)
        );
        """,
        # Down migration
        """
        DROP TABLE IF EXISTS badges;
        """,
    ]
]
