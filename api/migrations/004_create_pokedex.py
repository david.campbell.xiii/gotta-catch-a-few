steps = [
    [  # Up migration
        """
        CREATE TABLE IF NOT EXISTS pokedex (
            id  SERIAL UNIQUE NOT NULL,
            name varchar(20)   NOT NULL,
            lvl int  NOT NULL,
            hp int  NOT NULL,
            dfns int  NOT NULL,
            atk int  NOT NULL,
            spd int  NOT NULL,
            element varchar(20)  NOT NULL,
            caught boolean  NOT NULL,
            sprite_url_front varchar(200)  NOT NULL,
            sprite_url_back varchar(200)  NOT NULL,
            CONSTRAINT pk_pokedex PRIMARY KEY (id)
        );
        """,
        # Down migration
        """
        DROP TABLE IF EXISTS pokedex;
        """,
    ]
]
