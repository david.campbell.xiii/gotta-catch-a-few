steps = [
    [
        """
        INSERT INTO badges (badge_name, sprite_url)
        VALUES
            ('Copper Badge', 'https://example.com/badge1.png'),
            ('Iron Badge', 'https://example.com/badge2.png'),
            ('Sadge Badge', 'https://example.com/badge3.png');
        """,
        """
        DELETE FROM badges
        WHERE badge_name IN ('Copper Badge', 'Iron Badge', 'Sadge Badge');
        """,
    ],
    [
        """
        INSERT INTO special_moves (name, element, dmg)
        VALUES
            ('Sharp Leaf', 'grass', 10),
            ('Fire Fighting', 'fire', 12),
            ('Water Balloon', 'water', 15);
        """,
        """
        DELETE FROM special_moves WHERE name IN ('Sharp Leaf', 'Fire Fighting', 'Water Balloon');
        """,
    ],
    [
        """
        INSERT INTO armor (name, def, durability, max_durability)
        VALUES
            ('Fur Armor', 10, 20, 20),
            ('Iron Armor', 20, 40, 40),
            ('Steel Armor', 99, 99, 99);
        """,
        """
        DELETE FROM armor WHERE name IN (
            'Fur Armor',
            'Iron Armor',
            'Steel Armor'
        );
        """,
    ],
    [
        """
        INSERT INTO helms (name, def, durability, max_durability)
        VALUES
            ('Fur Helm', 10, 20, 20),
            ('Iron Helm', 20, 40, 40),
            ('Steel Helm', 99, 99, 99);
        """,
        """
        DELETE FROM helms WHERE name IN (
            'Fur Helm',
            'Iron Helm',
            'Steel Helm'
        );
        """,
    ],
    [
        """
        INSERT INTO boots (name, def, durability, max_durability)
        VALUES
            ('Fur Boots', 10, 20, 20),
            ('Iron Boots', 20, 40, 40),
            ('Steel Boots', 99, 99, 99);
        """,
        """
        DELETE FROM boots
        WHERE name IN('Fur Boots', 'Iron Boots', 'Steel Boots');
        """,
    ],
    [
        """
        INSERT INTO weapons (name, atk, durability, max_durability)
        VALUES
            ('Iron Sword', 10, 20, 20),
            ('Dragontail Sword', 20, 40, 40),
            ('I HAVE THE POWER', 99, 99, 99);
        """,
        """
        DELETE FROM weapons WHERE name IN (
            'Fur Sword',
            'Dragontail Sword',
            'I HAVE THE POWER'
        );
        """,
    ],
]
