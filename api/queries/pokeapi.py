from queries.client import pool
from psycopg.rows import dict_row
import httpx


# this fetches the first 151 in a list
async def fetch_pokedex_data():
    url = "https://pokeapi.co/api/v2/pokemon/"
    async with httpx.AsyncClient() as client:
        response = await client.get(url, params={"limit": 151})
        print(response)
        return response.json()["results"]

    # so we need to use that list and get the individual details


async def fetch_pokemon_details(url):
    async with httpx.AsyncClient() as client:
        response = await client.get(url)
        print(response)
        return response.json()


async def insertion():
    poke_list = await fetch_pokedex_data()
    for pokemon in poke_list:
        details = await fetch_pokemon_details(pokemon["url"])
        if details:
            insert_pokedex(details)


def insert_pokedex(data):
    try:
        # MATT CODE ROCKS
        name = data["name"]
        lvl = 1
        hp = data["stats"][0]["base_stat"]
        attack = data["stats"][1]["base_stat"]
        defense = data["stats"][2]["base_stat"]
        speed = data["stats"][5]["base_stat"]
        element = data["types"][0]["type"]["name"]
        caught = False
        sprite_url_back = data["sprites"]["other"]["showdown"]["back_default"]
        sprite_url_front = data["sprites"]["other"]["showdown"][
            "front_default"
        ]

        with pool.connection() as conn:
            with conn.cursor(row_factory=dict_row) as cur:
                cur.execute(
                    """
                    INSERT INTO pokedex (
                        name,
                        lvl,
                        hp,
                        atk,
                        dfns,
                        spd,
                        element,
                        caught,
                        sprite_url_back,
                        sprite_url_front
                        )
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                    """,
                    [
                        name,
                        lvl,
                        hp,
                        attack,
                        defense,
                        speed,
                        element,
                        caught,
                        sprite_url_back,
                        sprite_url_front,
                    ],
                )
            conn.commit()
    except Exception as error:
        print(f"An error occurred: {error}")
