from queries.client import get_api_connection
from models import Ball
from fastapi import HTTPException
from psycopg.rows import dict_row


async def get_ball(ball_type: str) -> Ball | None:
    async with get_api_connection() as conn:
        async with conn.cursor(row_factory=dict_row) as cur:
            query = """
                SELECT id, ball_type, cost, catch_rate
                FROM balls
                WHERE ball_type = %s;
            """
            await cur.execute(query, (ball_type,))
            row = await cur.fetchone()
            if row:
                return row["ball_type"]
            else:
                return None


"""
Get a random pokemon id only. This is for the Gotcha encounter where we can use
the ID to pull from the pokedex in other places, e.g. we can use the ID to add
a pokedex row to the poke instances table
"""


async def get_random_pokemon():
    async with get_api_connection() as conn:
        async with conn.cursor(row_factory=dict_row) as cur:
            query = """
                SELECT
                id,
                name,
                lvl,
                hp,
                dfns,
                atk,
                spd,
                element,
                sprite_url_front,
                sprite_url_back
                FROM pokedex
                ORDER BY RANDOM() LIMIT 1
            """
            await cur.execute(
                query,
            )
            random_pokemon = await cur.fetchone()
            if random_pokemon:
                return random_pokemon
            else:
                raise HTTPException(status_code=404, detail="No Pokemon Found")
