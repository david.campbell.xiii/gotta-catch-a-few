from fastapi import APIRouter, HTTPException
from queries.utility import get_ball


router = APIRouter()


@router.get("/api/balls/{ball_type}", response_model=str)
async def get_pokeball(ball_type: str):
    request = await get_ball(ball_type)
    if request is None:
        raise HTTPException(status_code=404, detail="Ball not found")
    return request
