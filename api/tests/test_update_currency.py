from fastapi.testclient import TestClient
from queries.trainers import TrainersQueries
from authenticator import authenticator
from main import app


client = TestClient(app)


def mock_get_current_account_data():
    return {"id": 99, "username": "StevieWonder"}


class MockTrainersQueries:
    async def update_trainer_currency(self, trainer_id: int, currency: float):
        return True


def test_update_currency_success():
    # Arrange
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = mock_get_current_account_data
    app.dependency_overrides[TrainersQueries] = MockTrainersQueries

    # Act
    # PUT the cash to the endpoint
    response = client.put("/api/trainers/currency", json={"currency": 9999})

    # Assert
    # Check , and expected reply
    assert response.status_code == 200
    assert response.json() == {"message": "Currency updated successfully."}
