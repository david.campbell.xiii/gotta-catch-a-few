import { AuthProvider } from '@galvanize-inc/jwtdown-for-react'
import { createBrowserRouter, RouterProvider, Outlet } from 'react-router-dom'
import Layout from './Layout'
import NavBar from './NavBar'
import LoginPage from './pages/LoginPage'
import SignUpPage from './pages/SignUpPage'
import ProfilePage from './pages/ProfilePage'
import GotchaPage from './pages/GotchaPage'
import { profileLoader } from '../loaders'
import './index.css'
import EncounterPage from './pages/EncounterPage'
import CapturePage from './pages/CapturePage'

// To Deploy:
// 1. In api/main.py:
//     - change 'if os.environ.get("DEV_ENV") == 'True':' to 'False'

// 2. In ghi/app.jsx:
//     - uncomment 'const domain' and 'const baseUrl' and change
//     - <AuthProvider baseUrl={API_HOST}> to <AuthProvider baseUrl={baseUrl}>

// 3. In Top level .env:
//     - change DEV_ENV=True to DEV_ENV=False

const API_HOST = import.meta.env.VITE_API_HOST
// // const domain = /https:\/\/[^/]+/
// // const baseUrl = import.meta.env.VITE_PUBLIC_URL.replace(domain, '')

if (!API_HOST) {
    throw new Error('VITE_API_HOST is not defined')
}

function App() {
    const router = createBrowserRouter([
        {
            path: '/',
            element: (
                <>
                    <Layout>
                        <NavBar />
                        <Outlet />
                    </Layout>
                </>
            ),
            children: [
                { index: true, element: <LoginPage /> },
                { path: 'signup', element: <SignUpPage /> },
                {
                    path: 'profile',
                    element: <ProfilePage />,
                    loader: profileLoader,
                },
                { path: 'gotcha', element: <GotchaPage /> },
                { path: 'profile', element: <ProfilePage /> },
                { path: 'encounter', element: <EncounterPage /> },
                { path: 'capture', element: <CapturePage /> },
            ],
        },
    ])

    return ( //NEED TO CHANGE THIS PRIOR TO MERGE MAIN BACK TO baseUrl
        <AuthProvider baseUrl={API_HOST}>
            <RouterProvider router={router} />
        </AuthProvider>
    )
}

export default App
