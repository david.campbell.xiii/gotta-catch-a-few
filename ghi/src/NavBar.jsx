import { NavLink, useLocation, useNavigate } from 'react-router-dom'
import { useState, useEffect } from 'react'
import { getAccountInfo, getLoginStatus } from './services/api/getServices'
import { Button } from '@/components/ui/button'
import {
    Accordion,
    AccordionContent,
    AccordionItem,
    AccordionTrigger,
} from '@/components/ui/accordion'
import { logoutCookie } from './services/api/modServices'

function NavBar() {
    const location = useLocation()
    const [user, setUser] = useState(null)
    const navigate = useNavigate()

    const checkLoginStatus = async () => {
        try {
            const loggedIn = await getLoginStatus()
            setUser(loggedIn ? await getAccountInfo() : null)
        } catch (error) {
            alert('Error checking login status:', error)
        }
    }

    useEffect(() => {
        checkLoginStatus()
    }, [location])

    const handleLogout = (e) => {
        e.preventDefault()
        logoutCookie()
        setUser(null)
        navigate('/')
    }

    return (
        <div className="fixed top-[0] bottom-[0] left-[0] w-[150px] overflow-auto p-4">
            <div className="fixed w-[125px] top-[0] bottom-[0] left-[0] bg-[#040722] shadow-sm rounded">
                <ul>
                    {!user && (
                        <li className="pt-2 pl-3">
                            <NavLink
                                className="nav-link"
                                activeclassname="active"
                                to="/"
                            >
                                Login
                            </NavLink>
                        </li>
                    )}
                    {user && (
                        <li className="pl-3">
                            <Accordion type="single" collapsible>
                                <AccordionItem
                                    className="accordion-item"
                                    value="trainer"
                                >
                                    <AccordionTrigger className="nav-link">
                                        Trainer
                                    </AccordionTrigger>
                                    <AccordionContent>
                                        <ul>
                                            <li>
                                                <NavLink
                                                    className="nav-link"
                                                    activeclassname="active"
                                                    to="/profile"
                                                >
                                                    Profile
                                                </NavLink>
                                            </li>
                                        </ul>
                                    </AccordionContent>
                                </AccordionItem>
                            </Accordion>
                        </li>
                    )}
                    {user && (
                        <li className="pl-3">
                            <NavLink
                                className="nav-link"
                                activeclassname="active"
                                to="/gotcha"
                            >
                                GOTCHA
                            </NavLink>
                        </li>
                    )}
                </ul>
                {user && (
                    <Button
                        variant="destructive"
                        className="absolute bottom-4 justify-center rounded w-full"
                        type="submit"
                        value="logout"
                        onClick={handleLogout}
                    >
                        Logout
                    </Button>
                )}
            </div>
        </div>
    )
}

export default NavBar
