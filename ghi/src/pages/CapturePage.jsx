import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { getProfileInfo } from '../services/api/getServices'
import { attemptCatch } from '@/services/api/modServices'
import { Button } from '@/components/ui/button'
import GreatBallImage from '@/images/GreatBall_Sprite.png'
import SafariBallImage from '@/images/SafariBall_Sprite.png'
import MasterBallImage from '@/images/MasterBall_Sprite.png'
import PokeBallImage from '@/images/PokeBall_Sprite.png'
import UltraBallImage from '@/images/UltraBall_Sprite.png'
import bugBG from '/bugBG.png'
import dragonBG from '/dragonBG.png'
import electricBG from '/electricBG.png'
import fairyBG from '/fairyBG.png'
import fightingBG from '/fightingBG.png'
import fireBG from '/fireBG.png'
import flyingBG from '/flyingBG.png'
import ghostBG from '/ghostBG.png'
import grassBG from '/grassBG.png'
import groundBG from '/groundBG.png'
import iceBG from '/iceBG.png'
import normalBG from '/normalBG.png'
import poisonBG from '/poisonBG.png'
import psychicBG from '/psychicBG.png'
import rockBG from '/rockBG.png'
import waterBG from '/waterBG.png'
import { useToast } from '@/components/ui/use-toast'
import {
    Card,
    CardContent,
    CardDescription,
    CardHeader,
    CardTitle,
} from '@/components/ui/card'
import {
    Carousel,
    CarouselContent,
    CarouselItem,
    CarouselNext,
    CarouselPrevious,
} from '@/components/ui/carousel'

const BALL_TYPES = [
    { name: 'SafariBall', cost: 0, image: SafariBallImage },
    { name: 'PokeBall', cost: 10, image: PokeBallImage },
    { name: 'GreatBall', cost: 15, image: GreatBallImage },
    { name: 'UltraBall', cost: 25, image: UltraBallImage },
    { name: 'MasterBall', cost: 60, image: MasterBallImage },
]

function CapturePage() {
    const navigate = useNavigate()
    const [encounteredPokemon, setEncounteredPokemon] = useState(null)
    const [gotchaData, setGotchaData] = useState(null)
    const [profileData, setProfileData] = useState(null)
    const [throwCount, setThrowCount] = useState(0)
    const capitalizeFirstLetter = (string) => {
        return string.charAt(0).toUpperCase() + string.slice(1)
    }
    const getBackgroundImage = (element) => {
        switch (element) {
            case 'bug':
                return bugBG
            case 'dragon':
                return dragonBG
            case 'electric':
                return electricBG
            case 'fairy':
                return fairyBG
            case 'fighting':
                return fightingBG
            case 'fire':
                return fireBG
            case 'flying':
                return flyingBG
            case 'ghost':
                return ghostBG
            case 'grass':
                return grassBG
            case 'ground':
                return groundBG
            case 'ice':
                return iceBG
            case 'normal':
                return normalBG
            case 'poison':
                return poisonBG
            case 'psychic':
                return psychicBG
            case 'rock':
                return rockBG
            case 'water':
                return waterBG
            default:
                return ''
        }
    }
    const { toast } = useToast()

    useEffect(() => {
        // Loading Encounter page Pokemon and gotcha data from localStorage
        const storedPokemon = JSON.parse(
            localStorage.getItem('encounteredPokemon')
        )
        const storedGotchaData = JSON.parse(localStorage.getItem('gotchaData'))
        if (storedPokemon) setEncounteredPokemon(storedPokemon)
        if (storedGotchaData) setGotchaData(storedGotchaData)

        // Fetch profile data for the partyfull check
        const fetchProfileData = async () => {
            try {
                let profileData = await getProfileInfo()
                setProfileData(profileData)
            } catch (error) {
                console.error('Failed to fetch profile info:', error)
            }
        }

        fetchProfileData()
    }, [])

    const handleThrow = async (ballName) => {
        if (!gotchaData || !encounteredPokemon) {
            console.error('Gotcha data or encountered pokemon is not available')
            return
        }

        // Early exit if no balls of this type are left
        if (gotchaData.balls[ballName] <= 0) {
            console.error('No balls of this type left')
            return
        }

        // Update ball count and throw count
        gotchaData.balls[ballName] -= 1
        setThrowCount((prevCount) => prevCount + 1)
        localStorage.setItem('gotchaData', JSON.stringify(gotchaData))
        // Persist updated ball count

        try {
            const catchData = {
                pokemon_id: encounteredPokemon.id,
                ball_type: ballName,
            }
            const catchResult = await attemptCatch(catchData)

            // Handle successful catch
            if (catchResult.poke_instance_id) {
                await afterCatchSuccess()
                return // Exit after handling success
            }

            // Handle case after three throws or if no balls are left
            if (
                throwCount >= 2 ||
                !Object.values(gotchaData.balls).some((qty) => qty > 0)
            ) {
                await afterMaxThrowsOrNoBallsLeft()
            }
        } catch (error) {
            console.error('Error during catch attempt:', error)
        }
    }

    // Called after a successful catch
    const afterCatchSuccess = async () => {
        const updatedProfileData = await getProfileInfo()
        setProfileData(updatedProfileData)

        toast({
            variant: 'success',
            title: 'Catch Successful!',
            description: `${capitalizeFirstLetter(
                encounteredPokemon.name
            )} was caught successfully.`,
        })

        // Decide on navigation based on party size and balls left
        if (updatedProfileData.length < 3) {
            localStorage.removeItem('encounteredPokemon')
            // Clear encounteredPokemon for the next encounter
            navigate('/encounter')
        } else {
            // Party is full, clear local storage and navigate to profile
            localStorage.clear()
            navigate('/profile')
        }
    }

    // Called after three throws or no balls left without a catch
    const afterMaxThrowsOrNoBallsLeft = async () => {
        // Fetch updated profile information for a fresh check
        const updatedProfileData = await getProfileInfo()
        const isPartyFull = updatedProfileData.length >= 3
        const areBallsLeft = Object.values(gotchaData.balls).some(
            (qty) => qty > 0
        )

        if (!isPartyFull && areBallsLeft) {
            // If there are balls left and party isn't full, go back to encounter
            localStorage.removeItem('encounteredPokemon') // Clear only encounteredPokemon
            toast({
                variant: 'success',
                title: 'Womp Womp...',
                description: `${capitalizeFirstLetter(
                    encounteredPokemon.name
                )} ran away!`,
            })

            navigate('/encounter')
        } else if (!areBallsLeft) {
            // If no balls left, redirect to gotcha immediately
            navigate('/gotcha')
            localStorage.clear() // Clear local storage as a significant transition occurs
        } else {
            // Party is full, go to gotcha or profile page accordingly
            navigate('/gotcha')
            localStorage.clear() // Clear local storage as a significant transition occurs
        }
    }

    return (
        <>
            <div className="flex flex-col gap-4 ml-[125px] p-4">
                <div className="flex-grow">
                    <div className="flex flex-col items-center justify-center w-full">
                        <div className="pb-3">
                            {encounteredPokemon ? (
                                <>
                                    <div
                                        className="relative shadow-lg shadow-secondary  h-[400px]"
                                        style={
                                            encounteredPokemon
                                                ? {
                                                      backgroundImage: `url(${getBackgroundImage(
                                                          encounteredPokemon.element
                                                      )})`,
                                                      backgroundSize: 'cover',
                                                      backgroundPosition:
                                                          'center',
                                                  }
                                                : {}
                                        }
                                    >
                                        <img
                                            className="absolute top-3/4 left-1/2 transform -translate-x-1/2 -translate-y-1/2 w-full h-[400px] object-scale-down rounded"
                                            src={
                                                encounteredPokemon.sprite_url_front
                                            }
                                            alt={encounteredPokemon.name}
                                        />
                                    </div>
                                    <Card className="w-[450px] border-[2px] border-[solid] border-[#8cd4d7] bg-[#373c44] rounded-[10px] [box-shadow:0_4px_6px_rgba(0,_0,_0,_0.1)] mb-4">
                                        <CardHeader>
                                            <CardTitle className="font-bold">
                                                Name:{' '}
                                                {capitalizeFirstLetter(
                                                    encounteredPokemon.name
                                                )}
                                            </CardTitle>
                                            <CardDescription>
                                                Level {encounteredPokemon.lvl}
                                            </CardDescription>
                                        </CardHeader>
                                        <CardContent>
                                            <div className="overflow-hidden border-[2px] border-[solid] border-[#8cd4d7] bg-[#373c44] rounded-[10px]">
                                                <table className="min-w-full table-fixed">
                                                    <thead>
                                                        <tr>
                                                            <th className="w-1/4 py-2"></th>
                                                            <th className="w-1/4 py-2"></th>
                                                            <th className="w-1/4 py-2"></th>
                                                            <th className="w-1/4 py-2"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td className="font-bold px-6 py-4 whitespace-normal text-right">
                                                                HP :
                                                            </td>
                                                            <td className="px-6 py-4 whitespace-normal text-left">
                                                                {
                                                                    encounteredPokemon.hp
                                                                }
                                                            </td>
                                                            <td className="font-bold px-6 py-4 whitespace-normal text-right">
                                                                SPD :
                                                            </td>
                                                            <td className="px-6 py-4 whitespace-normal text-left">
                                                                {
                                                                    encounteredPokemon.spd
                                                                }
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td className="font-bold px-6 py-4 whitespace-normal text-right">
                                                                ATK :
                                                            </td>
                                                            <td className="px-6 py-4 whitespace-normal text-left">
                                                                {
                                                                    encounteredPokemon.atk
                                                                }
                                                            </td>
                                                            <td className="font-bold px-6 py-4 whitespace-normal text-right">
                                                                DFNS :
                                                            </td>
                                                            <td className="px-6 py-4 whitespace-normal text-left">
                                                                {
                                                                    encounteredPokemon.dfns
                                                                }
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td
                                                                className="text-right font-bold px-6 py-4 whitespace-normal"
                                                                colSpan={2}
                                                            >
                                                                TYPE :
                                                            </td>
                                                            <td
                                                                className="px-6 py-4 whitespace-normal text-left"
                                                                colSpan={2}
                                                            >
                                                                {capitalizeFirstLetter(
                                                                    encounteredPokemon.element
                                                                )}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </CardContent>
                                    </Card>
                                </>
                            ) : (
                                <p>Loading...</p>
                            )}
                        </div>
                        <Carousel className="justify-center w-[150px]">
                            <CarouselContent>
                                {BALL_TYPES.map((ball) => (
                                    <CarouselItem
                                        key={ball.name}
                                        className="flex justify-center items-center"
                                    >
                                        <div className="m-[10px]">
                                            <img
                                                src={ball.image}
                                                alt={ball.name}
                                            />
                                            <h3 className="justify-center font-bold">
                                                {ball.name}
                                            </h3>
                                            <p>
                                                Quantity:{' '}
                                                {gotchaData
                                                    ? gotchaData.balls[
                                                          ball.name
                                                      ]
                                                    : 0}
                                            </p>
                                            <Button
                                                className="rounded"
                                                variant="outline"
                                                disabled={
                                                    gotchaData
                                                        ? gotchaData.balls[
                                                              ball.name
                                                          ] <= 0
                                                        : true
                                                }
                                                onClick={() =>
                                                    handleThrow(ball.name)
                                                }
                                            >
                                                Throw
                                            </Button>
                                        </div>
                                    </CarouselItem>
                                ))}
                            </CarouselContent>
                            <CarouselNext />
                            <CarouselPrevious />
                        </Carousel>
                    </div>
                </div>
            </div>
        </>
    )
}

export default CapturePage
