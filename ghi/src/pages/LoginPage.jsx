import useToken from '@galvanize-inc/jwtdown-for-react'
import { Link, useNavigate } from 'react-router-dom'
import { useState } from 'react'
import { Button } from '@/components/ui/button'
import { Input } from '@/components/ui/input'
import { Label } from '@/components/ui/label'
import {
    Card,
    CardContent,
    CardDescription,
    CardFooter,
    CardHeader,
    CardTitle,
} from '@/components/ui/card'

const LoginForm = () => {
    const navigate = useNavigate()
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [loginError, setLoginError] = useState('');
    const { login } = useToken()


    const handleSubmit = async (e) => {
        e.preventDefault()
        const success = await login(username, password);
        if (!success) {
            setLoginError('Invalid username or password. Please try again.');
        } else {
            navigate('/profile')
        }
    };

    return (
        <>
            <div className="flex flex-col gap-4 ml-[125px] p-4">
                <div className="flex-grow">
                    <div className="flex flex-col items-center justify-center w-full">
                        <Card className="w-[300px]">
                            <CardHeader>
                                <CardTitle>Login</CardTitle>
                                <CardDescription>
                                    Login to Catch-a-few!
                                </CardDescription>
                            </CardHeader>
                            <CardContent>
                                {loginError && <div className="text-red-500 mb-8">{loginError}</div>}
                                <form onSubmit={(e) => handleSubmit(e)}>
                                    <div className="grid w-full items-center gap-4">
                                        <div className="flex flex-col space-y-1.5">
                                            <Label htmlFor="name">
                                                Trainer Name
                                            </Label>
                                            <Input
                                                required
                                                placeholder="Trainername"
                                                name="username"
                                                type="text"
                                                className="form-control rounded"
                                                onChange={(e) =>
                                                    setUsername(e.target.value)
                                                }
                                            />
                                        </div>
                                        <div className="flex flex-col space-y-1.5">
                                            <Label htmlFor="name">
                                                Password
                                            </Label>
                                            <Input
                                                required
                                                placeholder="Password"
                                                name="password"
                                                type="password"
                                                className="form-control rounded"
                                                onChange={(e) =>
                                                    setPassword(e.target.value)
                                                }
                                            />
                                        </div>
                                        <div className="flex justify-center">
                                            <Button
                                                className="rounded"
                                                type="submit"
                                                value="Login"
                                            >
                                                Login
                                            </Button>
                                        </div>
                                    </div>
                                </form>
                            </CardContent>
                            <CardFooter className="flex justify-center">
                                <p>
                                    No Account?{' '}
                                    <Link className="text-link" to="/signup">
                                        Sign up here
                                    </Link>
                                    .
                                </p>
                            </CardFooter>
                        </Card>
                    </div>
                </div>
            </div>
        </>
    )
}

export default LoginForm
