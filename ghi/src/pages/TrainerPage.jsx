import { useEffect, useState } from 'react'

const TrainerPage = () => {
    const [accountData, setAccountData] = useState(null)
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState(null)

    useEffect(() => {
        const fetchTrainerData = async () => {
            try {
                const response = await fetch('/api/trainers/profile/mine')
                if (!response.ok) {
                    throw new Error(`HTTP error! status: ${response.status}`)
                }
                const data = await response.json()
                setAccountData(data)
                setLoading(false)
            } catch (err) {
                setError(err.message)
                setLoading(false)
            }
        }

        fetchTrainerData()
    }, [])

    if (loading) {
        return <div>Loading...</div>
    }

    if (error) {
        return <div>Error: {error}</div>
    }

    return (
        <>
            <div>
                <h1>{accountData.trainer_name}'s Profile</h1>
                <p>Currency: {accountData.currency}</p>
                {/* Display other profile details as needed */}
            </div>
        </>
    )
}

export default TrainerPage
