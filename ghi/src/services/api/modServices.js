import axiosInstance from './axiosConfig'

export async function createAccount(accountData) {
    return axiosInstance
        .post(`/api/accounts`, accountData)
        .then((response) => response.data)
        .catch((error) => console.error('There was an error!', error))
}

export async function addPokemonToParty(pokemonData) {
    return axiosInstance
        .post('/api/pokemon/party/add-pokemon-to-party', pokemonData)
        .then((response) => response.data)
        .catch((error) => console.error('There was an error!', error))
}

export async function updateBattleOrder(orderData) {
    return axiosInstance
        .put(`/api/pokemon/party/update-battle-order`, orderData)
        .then((response) => response.data)
        .catch((error) => console.error('There was an error!', error))
}

export async function deletePokemon(pokeInstanceId) {
    return axiosInstance
        .delete(`/api/pokemon/${pokeInstanceId}`)
        .then((response) => response.data)
        .catch((error) => console.error('There was an error!', error))
}

export async function attemptCatch(catchData) {
    return axiosInstance
        .post(`/api/pokemon/attempt-catch`, catchData)
        .then((response) => response.data)
        .catch((error) => console.error('There was an error!', error))
}

export async function updateCurrency(currencyData) {
    return axiosInstance
        .put(`/api/trainers/currency`, currencyData)
        .then((response) => response.data)
        .catch((error) => console.error('There was an error!', error))
}

export function logoutCookie() {
    return axiosInstance
        .delete(`/token`)
        .then((response) => response.data)
        .catch((error) => console.error('There was an error!', error))
}
