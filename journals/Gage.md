## DAY 1

LOST DUE TO GIT stuff. live and learn...


## DAY 2

Today was particularly challenging. While working on the Docker Compose file with all its components, I encountered errors that seemed unique to my setup. These issues primarily involved Docker, followed by challenges with psycopg and PostgreSQL's reluctance to communicate with FastAPI. However, the most significant hurdle for me was configuring PgAdmin, which brought everything to a standstill. I couldn't help but feel that I was the cause of these problems.

To address this, Cambell, Matt, and I decided to refactor everything to ensure uniformity and eliminate any anomalies. We were 'mob coding.' Despite the fact that server registration worked smoothly for everyone else, I was confronted with a glaring red error: "UNABLE TO CONNECT TO SERVER: connection failed: FATAL: password authentication failed for user 'myuser'."

With our collective efforts in 'mob coding,' we were able to successfully address this issue and make progress, but I can't help but think that my current issues may have delayed our project.
